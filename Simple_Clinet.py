import socket
from PIL import Image
import io
import binascii


def main():
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    my_socket.connect(('127.0.0.1', 8820))
    while True:

        my_socket.settimeout(2)

        #send msg
        inp = input("choose TIME, RAND, NAME, TAKE_SCREENSHOT, DIR, DELETE, COPY:  ").strip()

        my_socket.send(inp.encode())

        if inp =='EXIT':
            break
        #recive msg
        chunk = my_socket.recv(4096)
        msg = chunk
        if inp == 'TAKE_SCREENSHOT':
            while chunk is not None:
                try:
                    chunk = my_socket.recv(4096)
                    msg += chunk
                except socket.timeout:
                    break
        if inp == 'DIR':
            print(chunk.decode().replace('\\\\\\', '\\'))
        else:
            print(chunk.decode())
        with open(r'./image.jpg', 'wb+') as f:
            f.write(msg)

    my_socket.close()
    print("The socket is closed :(")


if __name__ == '__main__':
    main()
