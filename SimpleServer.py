import socket
from datetime import datetime
import random
import pyautogui
import glob
import os
import shutil


def addLen(msg):
    return str(len(msg)).zfill(2)+msg


def take_screenshot():
    image= pyautogui.screenshot()
    image.save(r'./screen.jpg')
    binary_image_data = open(r'./screen.jpg', 'rb').read()
    os.remove(r'./screen.jpg')
    return binary_image_data

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('0.0.0.0', 8820))
server_socket.listen()
print("server is ready")
connect_socket, addres = server_socket.accept()
print("client connected")
while True:
    data = connect_socket.recv(1024).decode()
    print(data)
    if data=="TIME":
        connect_socket.send(addLen(str(datetime.now().now().strftime("%H:%M:%S"))).encode())

    elif data =="RAND":
        connect_socket.send(addLen(str(random.randint(0,10))).encode())

    elif data =="NAME":
        connect_socket.send(addLen("Steve chachashvili").encode())

    elif data == "TAKE_SCREENSHOT":
        image_data = take_screenshot()
        index = 0
        while index < len(image_data):  # sending in chunks
            print(image_data)
            connect_socket.send(image_data[index:index+1028])
            index += 1028

    elif data[:3] == "DIR":
        # DIR C:/
        file_dir = data[4:]
        connect_socket.send(str(glob.glob(file_dir + "\\*.*")).encode())

    elif data[:6] == "DELETE":
        file_name = data[7:]
        print(file_name)
        os.remove(file_name)
        connect_socket.send(("The file {"+file_name+"} have been deleted <3").encode())
    elif data[:4] == "COPY":
        files_names = data[5:].split(" ")
        shutil.copy(files_names[0], files_names[1])
        connect_socket.send(f'{files_names[0]} was copied to {files_names[0]}')
    elif data == "EXIT":
        break
    else:
        connect_socket.send(addLen("Wrong protocol").encode())

connect_socket.close()
server_socket.close()
print("Server is closed")
